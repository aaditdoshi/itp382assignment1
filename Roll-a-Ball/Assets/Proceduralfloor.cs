﻿using UnityEngine;
using System.Collections;

public class Proceduralfloor : MonoBehaviour {

    public GameObject floor;
    public Vector3 spawnVector;
    public float revolutionpercentage;
    public float createGap;
    private Vector3 pointofCreatiion;
    private bool hasCreated=false;

	// Use this for initialization
	void Start () {
      SpawnFLoorAroundMe();
	}
	
	// Update is called once per frame
	void Update () {

        if (!hasCreated)
        {
            SpawnFLoorAroundMe();
        }
        else
        {
          
            if((gameObject.transform.position-pointofCreatiion).magnitude>createGap)//if the ball has gone far enough from the creation point
            {
            
                hasCreated = false;
            }
        }
	}

    void SpawnFLoorAroundMe()
    {
        print(transform.position);
        print(((GameObject)(Instantiate(floor, transform.position, Quaternion.identity))).transform.position);
        for (int i = 0; i < 4; i++)
        {
            Instantiate(floor, transform.position + spawnVector, Quaternion.identity);
          
           spawnVector= Vector3.Cross(spawnVector, Vector3.up);
        }
        pointofCreatiion = gameObject.transform.position;
        hasCreated = true;
    }
}
