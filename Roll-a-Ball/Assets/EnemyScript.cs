﻿using UnityEngine;
using System.Collections;

public class EnemyScript : MonoBehaviour {

   
    TrailRenderer myRender;
    GameObject Player;
    public float Speed;
    float timer;
	// Use this for initialization
	void Start () {
        Player = GameObject.Find("Player");
        myRender = gameObject.GetComponent<TrailRenderer>();
        gameObject.GetComponent<Rigidbody>().AddForce((gameObject.transform.position - Player.transform.position)*10);
	}
	
	// Update is called once per frame
	void Update () {
      
    }

    void OnCollisionStay(Collision col)
    {
        if (col.gameObject.name == "Player")
        {
            gameObject.GetComponent<Rigidbody>().AddForce((gameObject.transform.position - Player.transform.position) * (1 / (gameObject.transform.position - Player.transform.position).magnitude));
        }
    }   
}
