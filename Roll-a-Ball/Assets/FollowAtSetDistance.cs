﻿using UnityEngine;
using System.Collections;

public class FollowAtSetDistance : MonoBehaviour {

    Vector3 FollowOffset;
    public Transform FollowedObject;
	// Use this for initialization
	void Start () {
        FollowOffset = gameObject.transform.position - FollowedObject.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = FollowedObject.position + FollowOffset;
	}
}
