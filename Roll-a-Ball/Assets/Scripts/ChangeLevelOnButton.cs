﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeLevelOnButton : MonoBehaviour {

    public string ChangeLevelTo;
    public KeyCode presskey;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	if(Input.GetKey(presskey))
        {
           
            SceneManager.LoadScene(ChangeLevelTo);
            
        }
	}
}
