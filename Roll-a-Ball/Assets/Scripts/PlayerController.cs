﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    private Rigidbody rb;
   private int count;

    public Text WinText;
    public Text countText;
    
    // Use this for initialization
    void Start() {
        count = 0;
        rb = GetComponent<Rigidbody>();
        ChangeScore();
        WinText.text = "";
    }

    // Update is called once per frame
    void Update() {

    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVerticle = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVerticle);

        rb.AddForce(movement*speed);
    }



    void OnTriggerEnter(Collider other)
    {
       if(other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count++;
            ChangeScore();
        }
    }

    void ChangeScore()
    {
        countText.text = "Count : " + count.ToString();
        if(count>=10)
        {
            WinText.text = "YOU WIN PRESS ENTER TO CONTINUE TO NEXT LEVEL";
        if(gameObject.GetComponent<ChangeLevelOnButton>())
            {
                gameObject.GetComponent<ChangeLevelOnButton>().enabled = true;
            }
        }
    }



   
}
