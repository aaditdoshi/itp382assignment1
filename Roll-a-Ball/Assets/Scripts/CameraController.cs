﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public GameObject player;

    private Vector3 Offset;
    private Vector3 newPosition;
    public float lerpRate;
    
	// Use this for initialization
	void Start () {
        Offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        newPosition = player.transform.position + Offset;
        transform.position = Vector3.Lerp(transform.position, newPosition, lerpRate);
	}
}
