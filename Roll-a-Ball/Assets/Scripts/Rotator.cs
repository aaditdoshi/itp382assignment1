﻿using UnityEngine;
using System.Collections;

public class Rotator : MonoBehaviour {


    public Vector3 rotateValue;
	// Update is called once per frame
	void Update () {
        transform.Rotate(rotateValue * Time.deltaTime);
	}
}
